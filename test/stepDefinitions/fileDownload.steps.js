import {defineSupportCode} from 'cucumber'
import fileDL from '../pageobjects/fileDownload.page'

defineSupportCode(function({Given}){
    Given(/^I download a .txt File$/,function(){
        fileDL.downloadTxtFile()
    })

    Given(/^I should print the content of the file on the console$/,function(){
        fileDL.printFileContent()
    })
})