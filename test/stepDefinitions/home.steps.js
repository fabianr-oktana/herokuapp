import { defineSupportCode } from 'cucumber';
import home from '../pageobjects/home.page';
defineSupportCode(function({ Given }) {


  Given(/^I navigato to heroku page$/, function() {
    home.open()
  });

  Given(/^I click on ([^"]*) link$/, function(linkText){
    home.goToLink(linkText)
  })

  
  });

  
  