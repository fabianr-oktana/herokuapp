import {defineSupportCode} from 'cucumber'
import auth from '../pageobjects/auth.page'

defineSupportCode(function({Given}){
    Given(/^I fill the username with ([^"]*)$/, function(user){
        auth.fillUsername(user)        
    }) 

    Given(/^I fill the password with ([^"]*)$/, function(pass){
        auth.fillPassword(pass)        
    }) 

    Given(/^I append '([^"]*)' to username$/, function(text){
        auth.appendToUser(text)        
    }) 

    Given(/^I should see an error$/, function(){
        expect(auth.getMsg()).to.equal("Your username is invalid!");    
    }) 

    Given(/^I click login button$/, function(){
        auth.clickLogIn()        
    }) 

    Given(/^I should log in successfully$/, function(){
        expect(auth.getMsg()).to.equal("You logged into a secure area!");    
    }) 
})