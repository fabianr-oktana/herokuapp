import {defineSupportCode} from 'cucumber'
import dynCont from '../pageobjects/dynamic.page'

defineSupportCode(function({Given}){
    Given(/^I log to the console the link to the avatar and text of each post, for the ([^"]*) time$/, function(time){
        dynCont.logAvatarLink(time)
        dynCont.logText(time)
    })

    Given(/^I take a screenshot$/, function(){
        dynCont.takeScrSht()
    })

    Given(/^I refresh the page$/, function(){
        browser.refresh()
    })
})