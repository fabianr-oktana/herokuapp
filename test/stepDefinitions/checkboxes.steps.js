import {defineSupportCode} from 'cucumber'
import checkB from '../pageobjects/checkboxes.page'

defineSupportCode(function({Given}){
    Given(/^I click on Checkbox ([^"]*)$/,function(cbNumb){
        checkB.clickOnCheckBox(cbNumb)
    })

    Given(/^I should see Checkbox ([0-9]) ([^"]*)$/,function(cbNumb,state){
        expect(checkB.verifyCheckBox(cbNumb,state),"Checkbox was not on the expected state").to.be.true;
    })


})