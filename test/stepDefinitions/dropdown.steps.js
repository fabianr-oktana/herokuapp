import {defineSupportCode} from 'cucumber'
import dropdown from '../pageobjects/dropdown.page'

defineSupportCode(function({Given}){
    Given(/^I select Option ([^"]*) on the dropdown menu$/,function(optNumb){
        dropdown.selectByOption(optNumb)
    })

    Given(/^I should see Option ([^"]*) selected$/,function(optNumb){
        expect(dropdown.dropdown.getValue()).to.equal(optNumb);
    })

})

