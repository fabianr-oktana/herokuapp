import {defineSupportCode} from 'cucumber'
import dragDrop from '../pageobjects/dragDrop.page'

defineSupportCode(function({Given}){
    Given(/^I drag box ([^"]) to box ([^"])$/,function(from,to){
        dragDrop.dragFromDropTo(from,to)
    })

    Given(/^I should see box ([^"]) first and box ([^"]) last$/,function(first,last){
        expect(dragDrop.firstCol.getText()).to.equal(first);
        expect(dragDrop.lastCol.getText()).to.equal(last);
    })

})

