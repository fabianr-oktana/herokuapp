Feature:  Exercise 4

Background: Go to heroku page
    Given I navigato to heroku page

Scenario Outline: Dropdown
    Given I click on Dropdown link
    When I select Option <number> on the dropdown menu
    Then I should see Option <number> selected
    Examples:
    | number |
    | 1 |
    | 2 |

Scenario: Authentication failure
    Given I click on Form Authentication link
    And I fill the username with user
    And I fill the password with admin
    And I append '@email.com' to username
    When I click login button
    Then I should see an error

Scenario: Authentication success
    Given I click on Form Authentication link
    And I fill the username with tomsmith
    And I fill the password with SuperSecretPassword!
    When I click login button
    Then I should log in successfully

Scenario: Checkbox
    Given I click on Checkboxes link
    When I click on Checkbox 1
    And I click on Checkbox 2
    Then I should see Checkbox 1 selected
    And I should see Checkbox 2 unselected

Scenario Outline: Dynamic content 
    Given I click on Dynamic Content link
    When I log to the console the link to the avatar and text of each post, for the <time> time
    Then I take a screenshot
    And I refresh the page
    Examples:
    | time |
    | first |
    | second |

Scenario: File Downloader
    Given I click on File Download link
    When I download a .txt File
    Then I should print the content of the file on the console

Scenario: Drag and drop
    Given I click on Drag and Drop link
    And I drag box A to box B
    And I drag box B to box A
    Then I should see box A first and box B last
    