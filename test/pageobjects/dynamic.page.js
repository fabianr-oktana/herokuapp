var TIME
class DynamicContent {

    get avatars() {return browser.$$('.row > div > img')}
    get texts() {return browser.$$('.example > .row > div > div > div:last-child')}

    logAvatarLink(time) {
        TIME = time
        console.log("\nLoggin the avatars source for the " + time + " time")
        let avtr = this.avatars
        for (let i in avtr) {
            console.log("Link to avatar " + i + ": \n" + avtr[i].getAttribute('src') + "\n")
        }
    }

    logText(time){
        console.log("\nLoggin the texts for the " + time + " time")
        let txt = this.texts
        for (let i in txt) {
            console.log("Text of row " + i + ": \n" + txt[i].getText() + "\n")
        }
    }

    takeScrSht() {
        browser.screenshot().saveScreenshot("./test/shots/scrsht" + TIME + "Time.png")
    }

}
export default new DynamicContent()