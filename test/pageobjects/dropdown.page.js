class dropdown {
    get dropdown() {return browser.element('#dropdown')}

    selectByOption(optNumb){
        this.dropdown.waitForVisible(5000)
        this.dropdown.selectByValue(optNumb)
    }
}
export default new dropdown()