import logger from '../../utilities/common-utilities';

class HomePage {

    get welcome()   { return browser.element('h1=Welcome to the-internet'); }

    open () {
        browser.url('http://the-internet.herokuapp.com/');
        logger.info("navigating to 'http://the-internet.herokuapp.com/'")
        this.waitForHomePageToLoad();
    }
    
    goToLink (linkText) {
      let selector = "=" + linkText
      browser.element(selector).click()
    }

    waitForHomePageToLoad () {
      if(!this.welcome.isVisible()){
        this.welcome.waitForVisible(5000)
      }
    }

}

export default new HomePage()
