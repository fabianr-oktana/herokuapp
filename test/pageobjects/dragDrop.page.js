
const fsExtra = require('fs-extra');
const path = process.cwd()

var dragAndDrop = require('html-dnd').codeForSelectors;

class DragDrop {
    
    get page() {return browser.element('h3=Drag and Drop')}
    get firstCol() {return browser.$$('.column')[0]}
    get lastCol() {return browser.$$('.column')[1]}
    dragFromDropTo(from, to) {
        this.page.waitForVisible(5000)
        from = browser.element('#column-' + from.toLowerCase())
        to = browser.element('#column-' + to.toLowerCase())
        //Using library
        browser.execute(dragAndDrop, from.selector, to.selector)

        //Using script
        // let script = fsExtra.readFileSync(path + '/utilities/dnd.js','utf8')
        // browser.execute(script + "$('" + from.selector + "').simulateDragDrop({ dropTarget: '" + to.selector + "' });")
    }
}
export default new DragDrop()