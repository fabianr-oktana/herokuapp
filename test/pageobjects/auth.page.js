class Auth {

    get username() {return browser.element('#username')}
    get password() {return browser.element('#password')}
    get loginButton() {return browser.element('.radius')}
    get msg() {return browser.element('.flash')}

    fillUsername(user) {
        this.username.waitForVisible(5000)
        this.username.setValue(user)
    }

    fillPassword(pass) {
        this.password.setValue(pass)
    }

    appendToUser(text) {
        this.username.addValue(text)
    } 

    clickLogIn() {
        this.loginButton.click()
    }

    getMsg() {
        this.msg.waitForVisible(5000)
        return this.msg.getText().trim().replace("\n×","")
    }

}
export default new Auth()