var FILE_NAME
var path = require('path');
const pathToDownload = path.resolve('./chromeDownloads');
const fsExtra = require('fs-extra');

class FileDownload {

    get page() {return browser.element('.example')}
    get txtFiles() {return browser.$$('*=.txt')}

    downloadTxtFile() {
        this.page.waitForVisible(5000)
        fsExtra.removeSync(pathToDownload)
        fsExtra.mkdirsSync(pathToDownload)
        let files = this.txtFiles
        let random = Math.floor(Math.random() * files.length)
        FILE_NAME = pathToDownload + "/" + files[random].getText()
        files[random].click()
    }

    printFileContent() { 
        console.log("content of" + FILE_NAME + ":\n")
        //waits for the file to exist in the folder, other wise would try to read before it even exists 
        while (!fsExtra.pathExistsSync(FILE_NAME)){ 
            browser.pause(100)          
        }
        console.log(fsExtra.readFileSync(FILE_NAME,'utf8'))
    }
}
export default new FileDownload()