class checkboxes {
    get checkboxes() {return browser.$$('#checkboxes > input')}

    clickOnCheckBox (cbNumb){
        this.checkboxes[0].waitForVisible(5000)
        this.checkboxes[cbNumb-1].click()
    }

    verifyCheckBox (cbNum,state) {
        let result = this.checkboxes[cbNum-1].isSelected()
        switch (state){
            case "selected":
                return result
                break
            case "unselected":
                return !result
                break
        }
    }
    
}
export default new checkboxes()